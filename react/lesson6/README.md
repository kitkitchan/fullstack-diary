# NOTE
[Open the note here](https://docs.google.com/presentation/d/1zCf77jH9tQZp-QmhdIVuhVvVRCT9YJ81JT1l8M-dodA/edit?usp=sharing)
# Tutorial
### Aims
- familiar with react router

### TODO
1. clone the starter project
2. try to view the pages
3. try to add router to the project
```
/ -> HomePage
/profile -> ProfilePage
/boy-friends -> BoyFriendsPage
/boy-friends/:id -> BoyFriendsPage contains BoyFriend
```