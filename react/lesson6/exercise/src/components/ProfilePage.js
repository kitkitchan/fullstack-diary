import React from "react";
import profile from "./profile.jpg";

function ProfilePage() {
  return (
    <div>
        <img src={profile} />
        <h1>世上的另一個我, 就是鏡內的我</h1>
        <div>
            身高: 40cm
        </div>
        <div>
            體重: 30kg
        </div>
    </div>
  );
}

export default ProfilePage;