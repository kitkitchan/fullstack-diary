import React from "react";

function HomePage() {
  return (
    <div>
      <h1>不要低頭低頭會掉下來</h1>
      <ul>
        <li>我並沒有對誰都好 如果我對你好 就只有你</li>
        <li>沒有什麼不公平，動了情就別想贏</li>
        <li>這個季節很乾凈 沒有故事沒有你</li>
        <li>一個女孩所能發的最大的脾氣 就是如同啞巴不開口說一句話</li>
        <li>男人要活得像頭獅子, 女人就要活得像頭雌性獅子</li>
        <li>唔怕貨比貨, 至怕貨比貨</li>
      </ul>
    </div>
  );
}

export default HomePage;