import React from "react";
import boyFriendList from "./info/boyFriendList";

function BoyFriend() {
  let boyFriendID = "KIT";
  let boyFriend = boyFriendList.find(
    (boyfriend) => boyfriend.id === boyFriendID
  );

  return (
    <div>
      <img src={boyFriend.profilePic} />
      <div>{`${boyFriend.nick}(${boyFriend.name})`}</div>
      <div>{boyFriend.feature}</div>
      <div>{boyFriend.height}</div>
      <div>{boyFriend.weight}</div>
    </div>
  );
}

export default BoyFriend;
