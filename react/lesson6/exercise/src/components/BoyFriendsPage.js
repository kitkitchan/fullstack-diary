import React from "react";
import boyFriendList from "./info/boyFriendList";
import BoyFriend from './BoyFriend';

function BoyFriendsPage() {
  return (
    <div>
      <h1>曾經傷過我的人</h1>
      <div style={{ display: "flex" }}>
        {boyFriendList.map((boyfriend) => {
          return (
            <div key={boyfriend.name} style={{ flex: 1 }}>
              <div>{boyfriend.name}</div>
            </div>
          );
        })}
      </div>
    </div>
  );
}

export default BoyFriendsPage;
