import hoProfile from './ho.jpg'
import kitProfile from './kit.png'
export default [
    {
        id:"HO",
        name:"浩爆",
        nick:"屯門項羽",
        feature:"好打得, 大力過項羽, 單手舉起自己",
        height:"160cm",
        weight:"50kg",
        profilePic:hoProfile
    },
    {
        id:"KIT",
        name:"傑",
        nick:"醉愛傑少",
        feature:"串過碼頭咕哩, 連續食兩支煙, 銀包用LV(正版)",
        height:"163cm",
        weight:"46kg",
        profilePic:kitProfile
    }
]