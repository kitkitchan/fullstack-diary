# NOTE
[Open the note here](https://docs.google.com/presentation/d/1ELETFs3Vu9-WY4eWqr1Bei5i_QeDkOGZR1I-AfDNdoI/edit?usp=sharing)
# Tutorial
### Aims
- familiar with rendering
- familiar with life cycle
### TODO
1. [clone the starter project](https://github.com/manyiu/react-20200914)
2. please follow the README.md to complete the exercise
3. [the completed demo page is right here](http://react20200914.s3-website.ap-east-1.amazonaws.com/)