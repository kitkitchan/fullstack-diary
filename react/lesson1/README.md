# NOTE
[Open the note here](https://docs.google.com/presentation/d/1i3CFRHDSz024rSxapP5mnRyaFw6ks6Jq_TKcJtWd-vI/edit?usp=sharing)
# Tutorial
### Aims
- familiar with Reactjs
- familiar with create-react-app
- familiar with state & component
### TODO
- convert the HTML version of chatroom to react
- try to break down the chatroom into component
- try to add a typing state with 100ms delay to the chatroom